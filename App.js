import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import Rotas from './Telas/Rotas';
export default function App(){
  return( 
  <NavigationContainer>
     <Rotas/>
  </NavigationContainer>
  )
}

