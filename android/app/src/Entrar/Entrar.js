import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Button} from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Button title="Entrar" onPress={this.entrar} />
        <Text style={{color: '#e5e5', fontSize: 28}}>Olá</Text>
        <Button title='sair' onPress={this.props.fechar}/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#DDD',
      height: '100%',
      width: '100%'
    },
  });
  