import {ImageComponent, Modal, StyleSheet} from 'react-native';
import {color} from 'react-native-elements/dist/helpers';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Modalestilo = StyleSheet.create({
  X: {
    marginTop: 10,
    backgroundColor: '#304ffe',
    width: 25,
    marginLeft: 15,
  },

  tela: {
    //tela principal
    backgroundColor: '#e5e5e5',
    flex: 1,
    resizeMode: 'cover',
  },
  box2: {
    //quadrado com a mensaguem de como vai
    width: '90%',
    height: 85,
    backgroundColor: '#e5e5e5',
    borderRadius: 18,
    alignItems: 'center',
    marginLeft: 18,
    marginTop: 20,
  },

  boxemoji: {
    //quadrado que vai ficar os emoji

    width: '90%',
    height: 70,
    backgroundColor: '#e5e5e5',
    flexDirection: 'row',
    borderRadius: 16,
    marginLeft: 17,
    marginTop: 13,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  msgEmoji: {
    color: '#000',
    fontSize: 10,
  },
  imagem: {
    borderRadius: 20,
    height: 40,
    width: 40,
  },

  touchable: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  container: {
    //quadrado com os emojis preto e branco
    backgroundColor: '#FFFFFF',
    width: '90%',
    height: 280,
    marginTop: 10,
    borderWidth: 1,
    fontSize: 12,
    borderRadius: 18,
    marginLeft: 17,
    alignItems: 'center',
  },

  box: {
    //frase
    width: '88%',
    height: '8%',
    backgroundColor: 'e5e5e5#',
    borderRadius: 20,
    top: 10,
  },

  Titulo: {
    fontSize: 20,
    alignItems: 'center',
    color: 'black',
    left: 20,
  },
  FlatList: {
    fontSize: 20,
    marginHorizontal: 20,
    marginVertical: 35,
  },

  icon: {
    // item de calendario
    textAlign: 'center',
    alignContent: 'center',
  },
  boxcomentario: {
    borderRadius: 18,
    borderWidth: 1,
    height: 60,
    width: 350,
    backgroundColor: '#FFFFFF',
    marginLeft: 19,
    marginTop: 20,
  },
  Msgcomentario: {
    color: '#969696',
    fontSize: 13,
    marginLeft: 10,
    marginTop: 10,
  },
  salvar: {
    borderRadius: 6,
    height: 50,
    backgroundColor: '#304FFE',
    width: 350,
    marginLeft: 20,
    marginTop: 20,
    justifyContent: 'center',
  },
  msgSalvar: {
    color: '#FFFFFF',
    marginLeft: 155,
    fontWeight: 'bold',
  },
});
export default Modalestilo;
