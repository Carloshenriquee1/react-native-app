import React from "react"
import { StyleSheet } from "react-native"

const flatList = StyleSheet.create({


textoFlatlist: {
    fontSize: 15,
    paddingHorizontal: 30,
    marginVertical: 10,
    flexDirection:'column'
  },
  estilobotaoflat:{
    flexDirection: "column",
    alignItems: "center",
    marginTop:10,
    
  },
  
  estiloFlat:{
    backgroundColor: '#ffffff',
    borderRadius: 30,
    width: 360,
    height: 290,
    marginVertical: 10,
    marginHorizontal: 10,
    borderWidth: 2,
    borderColor: '#4169e1',
  },

});

export default flatList