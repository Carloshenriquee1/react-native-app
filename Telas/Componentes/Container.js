import React from 'react';
import {Text, View, Image} from 'react-native';
import Dance from 'react-native-vector-icons/MaterialCommunityIcons';
import Esporte from 'react-native-vector-icons/MaterialIcons';
import Cozinha from 'react-native-vector-icons/MaterialIcons';
import Ponto1 from 'react-native-vector-icons/FontAwesome';

import estilo2 from '../Tela3/estilo2/estilotela3';

function Container({
  imagem,
  Data,
  humor,
  hrs,
  icon1,
  frase1,
  icon2,
  frase2,
  icon3,
  frase3,
  Msg,
  color,
}) {
  return (
    <View style={estilo2.container}>
      <View style={estilo2.box}>
        <Image style={estilo2.imagem} source={imagem} />
        <Text style={estilo2.TextoData}> {Data} </Text>
        <Text
          style={
            (estilo2.Textobem,
            {
              color: color,
              height: 37,
              width: 80,
              left: 92,
              fontWeight: '700',
              fontSize: 21,
              bottom: 50,
              top: -75,
            })
          }>
          {' '}
          {humor}{' '}
        </Text>
        <Text style={estilo2.TextoHora}> {hrs} </Text>
      </View>

      <View style={estilo2.box1}>
        <Dance style={estilo2.icons} name={icon1} color={'black'} size={20} />
        <Ponto1 style={estilo2.Ponto1} name="circle" color={'black'} size={4} />
        <Text style={estilo2.textoLado}>{frase1}</Text>

        <Esporte
          style={estilo2.icons2}
          name={icon2}
          color={'black'}
          size={20}
        />
        <Ponto1 style={estilo2.Ponto2} name="circle" color={'black'} size={4} />
        <Text style={estilo2.TxtEsporte}>{frase2}</Text>

        <Cozinha
          style={estilo2.icons3}
          name={icon3}
          color={'black'}
          size={20}
        />
        <Text style={estilo2.TxtCozinha}>{frase3}</Text>
      </View>

      <View style={estilo2.box2}>
        <Text style={estilo2.Frases}>{Msg}</Text>
      </View>
    </View>
  );
}
export default Container;
