import React from 'react';
import {StyleSheet, Text, View, TextInput, Image} from 'react-native';

function Tela2() {
  return (
    <View>
      <Image style={estilo.imagem} source={require('../../assets/neutral1.png')} />
    <Text style = {estilo.textStyles}>
    Você ainda não tem nenhum registro diário.
    Para começar, toque no ícone de adicionar na tela.
    </Text>
      </View>
  )}

  const estilo = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#e5e5e5',
    },
    textStyles: {
      fontSize: 18,
      textShadowColor:"#ACACAC",
      top:200,
      textAlign:"center",
      width: 300,
      left: 55
    },
    imagem: {
      width: 48,
      height: 48,
      top: 174,
      left: 185
    },
    },
  );
  
  export default Tela2;
  