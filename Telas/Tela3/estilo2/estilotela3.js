import {StyleSheet} from 'react-native';
const estilo2 = StyleSheet.create({
  tela: {
    backgroundColor: '#e5e5e5',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },

  container: {
    backgroundColor: '#FFFFFF',
    width: 350,
    height: 158,
    left: 22,
    marginTop: 20,
    borderRadius: 20,
  },

  box: {
    width: '100%',
    height: '50%',
    backgroundColor: '#FFFFFF',
    borderRadius: 20,
  },

  box1: {
    width: '100%',
    height: '20%',
    flexdirection: 'row',
    paddingLeft: 22,
    paddingTop: 5,
  },

  box2: {
    width: '100%',
    height: '30%',
    top: 20,
    alignContent: 'center',
    paddingLeft: 20,
  },

  textoLado: {
    top: -15,
    left: 40,
    bottom: 10,
    fontSize: 12,
    fontWeight: '900',
    color: 'black',
    fontWeight: 'bold',
    alignContent: 'center',
  },

  TxtEsporte: {
    top: -245,
    left: 133,
    bottom: 20,
    fontSize: 12,
    fontWeight: '900',
    color: 'black',
    fontWeight: 'bold',
    alignContent: 'center',
  },

  TxtCozinha: {
    top: -560,
    left: 248,
    bottom: 40,
    fontSize: 12,
    fontWeight: 'bold',
    fontWeight: '900',
    color: 'black',
    alignContent: 'center',
  },

  Textobem: {
    height: 37,
    width: 70,
    left: 92,
    fontWeight: '700',
    fontSize: 21,
    bottom: 50,
    top: -75,
    color: '#E24B4B',
  },
  TextoHora: {
    width: 40,
    top: -105,
    height: 21,
    color: '#ACACAC',
    fontSize: 14,
    lineHeight: 21,
    textAlign: 'center',
    left: 160,
    bottom: 63,
  },

  TextoData: {
    width: 400,
    height: 60,
    top: -35,
    left: 90,
    bottom: 100,
    fontSize: 13,
    alignContent: 'center',
  },

  Frases: {
    color: '#ACACAC',
    alignContent: 'center',
  },

  imagem: {
    width: 57,
    height: 57,
    resizeMode: 'cover',
    top: 20,
    left: 25,
  },

  icons: {
    width: 20,
    height: 20,
    top: 16,
    left: 10,
  },

  icons2: {
    width: 200,
    height: 200,
    top: -33,
    left: 100,
  },

  icons3: {
    width: 300,
    height: 300,
    top: -262,
    left: 210,
    bottom: 500,
  },
  Ponto1: {
    marginTop: 10.5,
    marginLeft: 90,
    top: -5,
  },
  Ponto2: {
    marginTop: 10.5,
    marginLeft: 190,
    top: -237,
  },
});
export default estilo2;
