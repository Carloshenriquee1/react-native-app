import Happy from '../../../assets/happy1.png.png'
import Sad from '../../../assets/sad.png.png'
import  Nervous from '../../../assets/nervous.png.png'


const Boxx = [
    {
        id: 1,
        imagem: Happy,
        Data: "HOJE, 23 DE JANEIRO",
        humor: "BEM",
        color: "red",
        hrs: "08:35",
        icon1 : "dance-ballroom" ,
        frase1 : "Festa",
        icon2 : "sports-handball",
        frase2 : "Esporte",
        icon3 : 'set-meal',
        frase3: "Cozinhar",
        Msg: "Hoje foi um dia muito bom."
    },
    {
        id: 2,
        imagem: Nervous,
        Data: "ONTEM, 22 DE JANEIRO",
        humor: "MAL",
        color: 'blue',
        hrs: "08:35",
        icon1 : "dance-ballroom" ,
        frase1 : "Festa",
        icon2 : "sports-handball",
        frase2 : "Esporte",
        icon3 : 'set-meal',
        frase3: "Cozinhar",
        Msg: "Lorem Ipsum Dolor Sit"
    },
    {
        id: 3,
        imagem: Sad,
        Data: "21 DE JANEIRO",
        humor: "TRISTE",
        color:"green",
        hrs: "08:35",
        icon1 : "dance-ballroom" ,
        frase1 : "Festa",
        icon2 : "sports-handball",
        frase2 : "Esporte",
        icon3 : 'set-meal',
        frase3: "Cozinhar",
        Msg: "Lorem Ipsum Dolor Sit"
    },
    {
        id: 4,
        imagem: Happy,
        Data: "20 DE JANEIRO",
        humor: "BEM",
        color: "red",
        hrs: "08:35",
        icon1 : "dance-ballroom" ,
        frase1 : "Festa",
        icon2 : "sports-handball",
        frase2 : "Esporte",
        icon3 : 'set-meal',
        frase3: "Cozinhar",
        Msg: "Lorem Ipsum Dolor Sit"
    },
    {
        id: 5,
        imagem: Happy,
        Data: "19 DE JANEIRO",
        humor: "BEM",
        color: "red",
        hrs: "08:35",
        icon1 : "dance-ballroom" ,
        frase1 : "Festa",
        icon2 : "sports-handball",
        frase2 : "Esporte",
        icon3 : 'set-meal',
        frase3: "Cozinhar",
        Msg: "Lorem Ipsum Dolor Sit"
    },
    {
        id: 6,
        imagem: Happy,
        Data: "18 DE JANEIRO",
        humor: "BEM",
        color: "red",
        hrs: "08:35",
        icon1 : "dance-ballroom" ,
        frase1 : "Festa",
        icon2 : "sports-handball",
        frase2 : "Esporte",
        icon3 : 'set-meal',
        frase3: "Cozinhar",
        Msg: "Lorem Ipsum Dolor Sit"
    },

]
export default Boxx