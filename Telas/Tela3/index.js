import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import estilo2 from '../Tela3/estilo2/estilotela3';
import Container from '../Componentes/Container';
import Boxx from './Boxx';

function Tela3({navigation}) {
  let itemSave = {};
  const [dados, setDados] = useState()
    const [carregando, setCarregando] = useState(true)

    useEffect(() => {//Pegando dados pela API
        async function getStorage() {
            await api.get("daily_entries?username=carloshenrique")
                .then(response => {
                    const data =  response.data
                    setDados(data)
                })
                .catch(error => console.log(error))
                .finally(() => setCarregando(false))
        }
        getStorage()

    }, [])
    console.warn(dados)
  return (
    <>
      <View style={estilo2.tela}>
        <FlatList
          data={Boxx}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                itemSave = item;
                navigation.navigate('Tela4', {itemSave});
              }}>
              <Container {...item} />
            </TouchableOpacity>
          )}
        />
      </View>
    </>
  );
}
export default Tela3;
