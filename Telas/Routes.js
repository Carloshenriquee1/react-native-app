import React from 'react';
import {
  createBottomTabNavigator,
  navigation,
} from '@react-navigation/bottom-tabs';
import Tela2 from './Tela2';
import Icone from 'react-native-vector-icons/Entypo';
import Icone2 from 'react-native-vector-icons/AntDesign';
import Rotas2 from './Rotas2';
import {View} from 'react-native';

const Tab = createBottomTabNavigator();
function Routes() {
  const Telafic = () => <View />;
  return (
    <Tab.Navigator screenOptions={{tabBarShowLabel: false, headerShown: false}}>
      <Tab.Screen
        options={{
          tabBarIcon: () => <Icone name="home" color={'blue'} size={24} />,
        }}
        name="Tela2"
        component={Tela2}
      />
      <Tab.Screen
        options={{
          tabBarIcon: () => (
            <Icone2 name="pluscircle" color={'blue'} size={44} />
          ),
        }}
        name="Telafic"
        component={Telafic}
        listeners={({navigation}) => ({
          tabPress: e => {
            e.preventDefault();
            navigation.navigate('Tela');
          },
        })}
      />

      <Tab.Screen
        options={{
          tabBarIcon: () => <Icone name="list" color={'blue'} size={24} />,
        }}
        name="Rotas2"
        component={Rotas2}
      />
    </Tab.Navigator>
  );
}

export default Routes;
