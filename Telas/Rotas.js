import React from 'react';

import {Component, NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './Login';
import Main from './Main';
import Tela from './TelaFinal/Tela';

const Stack = createNativeStackNavigator();

export default function Rotas() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{headerShown: false}}
        name="Login"
        component={Login}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Main"
        component={Main}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Tela"
        component={Tela}
      />
    </Stack.Navigator>
  );
}
