import React, {useState, useEffect} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import flatList from '../Modal/flatList';

export default function Icones({name}) {
  const [icones, setIcones] = useState();
  const [textos, setTextos] = useState();
  useEffect(() => {
    function selecaoIcones() {
      if (name === 'sports') {
        setIcones('sports-kabaddi');
        setTextos('Esportes');
      }
      if (name === 'shopping') {
        setIcones('shopping-cart');
        setTextos('Shopping');
      }
      if (name === 'rest') {
        setIcones('king-bed');
        setTextos('Descanso');
      }
      if (name === 'party') {
        setIcones('music-note');
        setTextos('Festa');
      }
      if (name === 'movies') {
        setIcones('local-movies');
        setTextos('Filmes');
      }
      if (name === 'good_meal') {
        setIcones('set-meal');
        setTextos('Refeição');
      }
      if (name === 'games') {
        setIcones('videogame-asset');
        setTextos('Jogos');
      }
      if (name === 'date') {
        setIcones('wc');
        setTextos('Encontro');
      }
      if (name === 'cooking') {
        setIcones('food-bank');
        setTextos('Cozinhar');
      }
    }
    selecaoIcones();
  }, []);
  return (
    <TouchableOpacity style={flatList.estilobotaoflat}>
      <MaterialIcons name={icones} color="black" size={25} />
      <Text style={flatList.textoFlatlist}>{textos}</Text>
    </TouchableOpacity>
  );
}
