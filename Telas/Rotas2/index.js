import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Tela3 from '../Tela3/';
import Tela4 from '../Tela4/index.js';
const Stack = createNativeStackNavigator();
function Rotas2() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{headerShown: false}}
        name="Tela3"
        component={Tela3}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="Tela4"
        component={Tela4}
      />
    </Stack.Navigator>
  );
}
export default Rotas2;
