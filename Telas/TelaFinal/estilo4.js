import react from 'react';
import {StyleSheet, View} from 'react-native';
import Relogio from 'react-native-vector-icons/MaterialCommunityIcons';

const estilo4 = StyleSheet.create({
  tela: {
    //tela principal
    backgroundColor: '#E5E5E5',
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  Botão: {
    width: 50,
    height: 50,
    backgroundColor: '#e5e5e5',
    top: 30,
    left: 20,
  },
  Setinha: {
    color: 'blue',
  },

  box2: {
    //tela de cima com hora, data e emoji
    width: '50%',
    height: '20%',
    marginTop: 10,
    backgroundColor: '#e5e5e5',
    borderRadius: 20,
    left: 95,
  },

  container: {
    //principal
    backgroundColor: '#FFFFFF',
    width: 350,
    height: 158,
    left: 22,
    marginTop: 50,
    borderRadius: 20,
  },

  box: {
    //frase
    width: '88%',
    height: '8%',
    marginTop: 20,
    backgroundColor: '#ffffff',
    marginLeft: 24,
    borderRadius: 20,
  },

  Texto: {
    //texto da box1
    width: '100%',
    color: '#000000',
    fontSize: 12,
    lineHeight: 20,
    textAlign: 'center',
  },
  textoData: {
    color: '#969696',
    textAlign: 'center',
  },
  textoHora: {
    color: '#969696',
    textAlign: 'center',
  },
  imagem: {
    width: 65,
    height: 65,
    resizeMode: 'cover',
    top: 20,
    left: 70,
  },
  Textobem: {
    height: 37,
    width: 70,
    left: 78,
    fontWeight: '700',
    fontSize: 18,
    bottom: 50,
    top: 20,
    color: '#E24B4B',
  }, //ICONES
  relogio1: {
    //item de relogio
    width: 18,
    height: 18,
    top: 33,
    left: 60,
  },

  calendario1: {
    // item de calendario
    width: 19,
    height: 19,
    top: 35,
    left: 7,
  },
  Dancing: {
    //item de festa
    width: 40,
    backgroundColor: 'blue',
    borderRadius: 500,
    marginLeft: 50,
    marginTop: 60,
  },
  TextoDancing: {
    height: 37,
    width: 70,
    marginLeft: 55,
    fontWeight: '700',
    fontSize: 12,
    bottom: 50,
    top: 20,
    color: '#000000',
  },
  Esportes: {
    //item de festa
    width: 40,
    backgroundColor: 'blue',
    borderRadius: 500,
    marginLeft: 160,
    marginTop: -80,
  },
  TextoEsportes: {
    height: 37,
    width: 70,
    marginLeft: 152,
    fontWeight: '700',
    fontSize: 12,
    marginTop: 23,
    color: '#000000',
  },
  Comida: {
    //item de festa
    width: 40,
    backgroundColor: 'blue',
    borderRadius: 500,
    marginLeft: 270,
    marginTop: -99,
  },
  TextoComida: {
    height: 37,
    width: 70,
    marginLeft: 265,
    fontWeight: '700',
    fontSize: 12,
    marginTop: 23,
    color: '#000000',
  },
});
export default estilo4;
