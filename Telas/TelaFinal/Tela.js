import React, {Component, useEffect, useState} from 'react';
import {
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  Modal,
  TextInput,
} from 'react-native';
import Api from '../../Servicos/api';
import Modalestilo from '../Modal/Modalestilo';
import Relogio from 'react-native-vector-icons/MaterialIcons';
import Calendario from 'react-native-vector-icons/MaterialIcons';
import Close from 'react-native-vector-icons/AntDesign';
import Icones from '../icones';



export default function Tela({navigation}) {
  const [data, setData] = useState('');
  const [hora, setHora] = useState('');
  const [atv, setAtv] = useState([]);
  function zeroFill(n) {
    return n < 10 ? `0${n}` : `${n}`;
  }
  useEffect(() => {
    let date = new Date().getDate()-1
    let year = new Date().getFullYear();
    let mouth = new Date().getMonth();
    let meses = new Array(
      'janeiro',
      'fevereiro',
      'março',
      'abril',
      'maio',
      'junho',
      'julho',
      'agosto',
      'setembro',
      'outubro',
      'novembro',
      'dezembro',
    );
    let hours = new Date().getHours()+21
    let min = zeroFill(new Date().getMinutes());
  
    setData('HOJE, ' + date + ' DE ' + meses[mouth].toUpperCase());
    setHora(hours + ':' + min);
  
    return () => {};
  }, []);




  





  const [atividades, setAtividades] = useState([]);
  useEffect(() => {
    async function getStorage() {
      Api.get('activities/')
        .then(response => {
          const data = response.data;
          setAtividades(data);
        })
        .catch(error => console(error));
    }
    getStorage();
  }, []);
  const [usuario, setUsuario1] = useState({
    daily_entry: {
      mood: 'sad',
      activity_ids: [5, 3, 1],
      description: 'hoje chorei',
      username: 'carloshenrique',
    },
  });
  function postApi(dailyentries) {
    Api.post('daily_entries/', dailyentries)
      .then(response => {
        const data = response.data;
        console.warn(data);
      })
      .catch(error => console.warn(error));
    ;
  }
  return (
    <>
      <Modal animationType="slide">
        <View style={Modalestilo.tela}>
          <View style={Modalestilo.X}>
            <TouchableOpacity onPress={() => navigation.goBack({})}>
              <Close name="close" size={23} />
            </TouchableOpacity>
          </View>

          <View style={Modalestilo.box2}>
            <Text style={Modalestilo.Titulo}> Como você está?</Text>
            <View>
              <Calendario name="calendar-today" size={10}></Calendario>
              <Text>{data}</Text>
              <Relogio name="timer" size={10}></Relogio>
              <Text>{hora}</Text>
            </View>
          </View>
          <View style={Modalestilo.boxemoji}>
            <TouchableOpacity style={Modalestilo.touchable}>
              <Image
                style={Modalestilo.imagem}
                source={require('../../assets/happy1.png.png')}
              />
              <Text style={Modalestilo.msgEmoji}>BEM</Text>
            </TouchableOpacity>

            <TouchableOpacity style={Modalestilo.touchable}>
              <Image
                style={Modalestilo.imagem}
                source={require('../../assets/nervous.png.png')}
              />
              <Text style={Modalestilo.msgEmoji}>CONFUSO</Text>
            </TouchableOpacity>

            <TouchableOpacity style={Modalestilo.touchable}>
              <Image
                style={Modalestilo.imagem}
                source={require('../../assets/sad2.png')}
              />
              <Text style={Modalestilo.msgEmoji}>TRISTE</Text>
            </TouchableOpacity>

            <TouchableOpacity style={Modalestilo.touchable}>
              <Image
                style={Modalestilo.imagem}
                source={require('../../assets/sleeping.png')}
              />
              <Text style={Modalestilo.msgEmoji}>SONO</Text>
            </TouchableOpacity>

            <TouchableOpacity style={Modalestilo.touchable}>
              <Image
                style={Modalestilo.imagem}
                source={require('../../assets/sad.png.png')}
              />
              <Text style={Modalestilo.msgEmoji}>MAL</Text>
            </TouchableOpacity>
          </View>

          <View style={Modalestilo.container}>
            <FlatList
              data={atividades}
              keyExtractor={item => item.id.toString()}
              numColumns={3}
              renderItem={({item}) => <Icones {...item} />}
            />
          </View>
          <View style={Modalestilo.boxcomentario}>
            <TextInput style={Modalestilo.Msgcomentario}>
              Escreva aqui oque aconteceu hoje...
            </TextInput>
          </View>
          <TouchableOpacity
            style={Modalestilo.salvar}
            onPress={() => {
              [postApi(usuario)];
            }}>
            <Text style={Modalestilo.msgSalvar}>SALVAR</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  );
}
