import React from 'react';
import {Text, View, Image,TouchableOpacity} from 'react-native';
import estilo4 from '../TelaFinal/estilo4';
import Relogio from 'react-native-vector-icons/MaterialIcons'
import Calendario from 'react-native-vector-icons/MaterialIcons'
import Dancing from 'react-native-vector-icons/MaterialCommunityIcons'
import Esportes from 'react-native-vector-icons/MaterialIcons'
import Comida from 'react-native-vector-icons/MaterialIcons'
import Setinha from 'react-native-vector-icons/AntDesign'
import { NavigationContainer } from '@react-navigation/native';



function Tela4({route:{params},navigation}) {
const itemSave = params.itemSave
const {
  imagem,
  Data,
  humor,
  hrs,
  icon1,
  frase1,
  icon2,
  frase2,
  icon3,
  frase3,
  Msg,
  color,
} = itemSave
  return (
    <View style={estilo4.tela}>
      <TouchableOpacity style={estilo4.Botão} onPress ={() => navigation.goBack() } ><Setinha style={estilo4.Setinha} name="left" color={'blue'} size={40} /></TouchableOpacity>
      <View style = {estilo4.box2}>
      <Relogio style={estilo4.relogio1} name="timer" color={'gray'} size={20} />
      <Calendario style={estilo4.calendario1} name="calendar-today" color={'gray'} size={20} />
      <Text style = {estilo4.textoHora}>{hrs}</Text>
        <Text style = {estilo4.textoData}>{Data}</Text>
        
       
        <Image style={estilo4.imagem} source={imagem} />
        <Text style = {[estilo4.Textobem, {color:color}]}>{humor}</Text>
      </View>


    <View style = {estilo4.container}>
    <Dancing style={estilo4.Dancing} name={icon1} color={'white'} size={40} />
    <Text style = {estilo4.TextoDancing}>{frase1}</Text>
    <Esportes style={estilo4.Esportes} name={icon2} color={'white'} size={40} />
    <Text style = {estilo4.TextoEsportes}>{frase2}</Text>
    <Comida style = {estilo4.Comida} name ={icon3} color={'white'} size= {40} />
    <Text style = {estilo4.TextoComida}>{frase3}</Text>

    </View>

    <View style = {estilo4.box}>
      <Text style={estilo4.Texto}>{Msg}
      
       </Text>

    </View>
      
    </View>
  );
}
export default Tela4;