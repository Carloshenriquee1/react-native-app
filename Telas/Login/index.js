/* eslint-disable prettier/prettier */
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Button} from 'react-native-elements';
import Eye from 'react-native-vector-icons/Feather';

function Login({navigation}) {
  const [input, setInput] = useState('');
  const [hidePass, setHidePass] = useState(true);
  return (
    <View style={estilo.container}>
      <Image
        style={estilo.tinyLogo}
        source={require('../../assets/logo.png')}
      />
      <Text style={estilo.textStyles} />

      <TextInput style={estilo.input} placeholder=" e-mail" />
      <View style = {estilo.area}>
        <TextInput
          style={estilo.textInputSenha}
          placeholder=" senha"
          value={input}
          onChangeText={texto => setInput(texto)}
          secureTextEntry={hidePass}></TextInput>
        <TouchableOpacity
          style={estilo.icon}
          onPress={() => setHidePass(!hidePass)}>
          {hidePass ? (
            <Eye name="eye" color="#000" size={20} />
          ) : (
            <Eye name="eye-off" color="#000" size={20} />
          )}
        </TouchableOpacity>
      </View>

      <Button
        title="ENTRAR"
        loading={false}
        loadingProps={{size: 'small', color: 'blue'}}
        buttonStyle={{
          backgroundColor: '#C6CEFF',
          borderRadius: 10,
        }}
        titleStyle={{fontWeight: 'bold', fontSize: 15}}
        containerStyle={{
          marginTop: 50,
          height: 46,
          width: 133,
          marginLeft: 10,
          padding: 5,
        }}
        onPress={() => navigation.navigate('Main')}
      />
    </View>
  );
}

const estilo = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#304FFE',
  },
  textStyles: {
    fontSize: 30,
  },
  logo: {
    width: 150,
    height: 200,
  },
  input: {
    marginTop: 15,
    width: 300,
    padding: 10,
    backgroundColor: '#fff',
    fontSize: 15,
    fontWeight: 'bold',
    borderRadius: 10,
  },
  area: {
    flexDirection: 'row',
    width: '76%',
    backgroundColor: '#F6F6F6',
    borderRadius: 12,
    height: 47,
    alignItems: 'center',
    marginTop: 15,
  },
  textInputSenha: {
    width: '85%',
    height: 50,
    padding: 8,
    fontSize: 15,
  },
  icon: {
    width: '15%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  
  },
});

export default Login;
